﻿
namespace RPN_Calculator.Interface.Parser
{
	public interface IPostFixParser
	{
		string ConvertToPostFix(string inputStatement);
	}
}
