﻿using System;
using System.Collections.Generic;
using System.Text;
using RPN_Calculator.Interface.Parser;
using RPN_Calculator.Interface.Common;
using RPN_Calculator.Common;
using RPN_Calculator.Common.Operator;

namespace RPN_Calculator.Parser
{
	public class PostFixParser : IPostFixParser
	{
		private readonly Stack<IOperator> opStack;
		private StringBuilder outputBuilder;
		private IOperatorFactory rpnOperatorFactory;
		private string functionBuffer;

		private PostFixParser() {}

		public PostFixParser(IOperatorFactory operatorFactory)
		{ 
			outputBuilder = new StringBuilder();
			opStack = new Stack<IOperator>();
			rpnOperatorFactory = operatorFactory;
			functionBuffer = "";
		}

		public string ConvertToPostFix(string inputStatement)
		{
			if (inputStatement.Length == 0)
				throw new EmptyInputStringException(Constants.Messages.Errors.EmptyInputString);

			outputBuilder.Clear();
			opStack.Clear();
			functionBuffer = "";

			inputStatement = inputStatement
				.ToLower()
				.Replace(Constants.Miscellaneous.RpnSeparator, "");

			foreach (char character in inputStatement)
			{
				var sCharacter = character.ToString();

				if (!Constants.OperatorSymbol.OperatorSymbols.Contains(sCharacter))
					SymbolHandler(sCharacter);

				else if (sCharacter.Equals(Constants.OperatorSymbol.BraceOpen))
					BraceOpenHandler();

				else if (sCharacter.Equals(Constants.OperatorSymbol.BraceClose))
					BraceCloseHandler();

				else // (!Constants.OperationSymbol.OperationSymbols.Contains(sCharacter))
					OperatorHandler(sCharacter);
			}
			return GetConvertedString();
		}

		private string GetConvertedString()
		{
			IOperator? rpnOperator = null;

			while(opStack.TryPop(out rpnOperator))
			{
				outputBuilder.Append(Constants.Miscellaneous.RpnSeparator);
				outputBuilder.Append(rpnOperator.Symbol);
			}

			return outputBuilder.ToString();
		}

		private void SymbolHandler(string symbol)
		{
			functionBuffer += symbol;
			outputBuilder.Append(symbol);
			if (Constants.FunctionSymbol.FunctionSymbols.Contains(functionBuffer))
			{
				var startIndexToRemove = outputBuilder.Length - functionBuffer.Length;
				outputBuilder = outputBuilder.Remove(startIndexToRemove, functionBuffer.Length);

				OperatorHandler(functionBuffer);
				
			}
		}

		private void BraceOpenHandler()
		{
			var openBrace = rpnOperatorFactory.GetOperator(Constants.OperatorSymbol.BraceOpen);
			opStack.Push(openBrace);
		}

		private void BraceCloseHandler()
		{
			IOperator? topToken = null;

			while(opStack.TryPop(out topToken) && 
				!topToken.Symbol.Equals(Constants.OperatorSymbol.BraceOpen))
			{
				outputBuilder.Append(Constants.Miscellaneous.RpnSeparator);
				outputBuilder.Append(topToken.Symbol);
			}
		}

		private void OperatorHandler(string sOperator)
		{
			var rpnOperator = rpnOperatorFactory.GetOperator(sOperator);
			IOperator? buffOperator = null;
			
			while(opStack.TryPeek(out buffOperator) &&
				buffOperator.Priority >= rpnOperator.Priority)
			{
				buffOperator = opStack.Pop();
				outputBuilder.Append(Constants.Miscellaneous.RpnSeparator);
				outputBuilder.Append(buffOperator.Symbol);
			}

			opStack.Push(rpnOperator);
			outputBuilder.Append(Constants.Miscellaneous.RpnSeparator);
			functionBuffer = "";
		}

	}
}
