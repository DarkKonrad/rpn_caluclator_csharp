﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using RPN_Calculator.Interface.Evaluator;

namespace RPN_Calculator.RpnEvaluator
{
	public class StepLogger : IPostFixStepLogger
	{
		private MemoryStream memoryStream;
		StreamWriter streamWriter;
		public StepLogger()
		{
			Initalize();
		}

		public void AddLog(params string[] messages)
		{
			foreach (var message in messages)
				streamWriter.Write(message);
			
			streamWriter.Flush();
		}

		public void ClearLogs()
		{
			Initalize();
		}

		public string GetLogs()
		{
			memoryStream.Position = 0;
			StreamReader reader = new StreamReader(memoryStream);
			return reader.ReadToEnd();
		}

		private void Initalize()
		{
			memoryStream = new MemoryStream();
			memoryStream.Position = 0;
			streamWriter = new StreamWriter(memoryStream);
		}
	}
}
