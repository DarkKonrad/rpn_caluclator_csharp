﻿
namespace RPN_Calculator.Interface.Evaluator
{
	public interface IPostFixStepLogger
	{
		void AddLog(params string[] messages);
		string GetLogs();
		void ClearLogs();
	}
}
