﻿using RPN_Calculator.Interface.Parser;
namespace RPN_Calculator.Interface.Evaluator
{
	public interface IPostFixEvaluator
	{
		double Evaluate(string InFixInput);
		IPostFixStepLogger Logger { get; set; }
		IPostFixParser Parser { get; set; }
	}
}
