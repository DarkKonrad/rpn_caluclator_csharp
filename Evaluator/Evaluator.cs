﻿using System;
using System.Collections.Generic;
using RPN_Calculator.Interface.Evaluator;
using RPN_Calculator.Interface.Parser;
using RPN_Calculator.Interface.Common;
using RPN_Calculator.Common;
using RPN_Calculator.Common.Operator;

using static RPN_Calculator.Common.Constants.Messages;


namespace RPN_Calculator.RpnEvaluator
{
	public class Evaluator : IPostFixEvaluator
	{
		private Stack<string> valuesStack;
		private IOperatorFactory operatorFactory;
		public IPostFixStepLogger? Logger { get; set; }
		public IPostFixParser Parser { get ; set ;}

		public Evaluator(IPostFixParser parser, IOperatorFactory operatorFactory, IPostFixStepLogger logger = null)
		{
			Logger = logger;
			Parser = parser;
			this.operatorFactory = operatorFactory;
			valuesStack = new Stack<string>();
		}

		public double Evaluate(string inFixInput)
		{
			if (inFixInput.Length == 0)
				throw new EmptyInputStringException(Errors.EmptyInputString);

			inFixInput = inFixInput.Replace('.', ',');
			valuesStack.Clear();
			
			var postFixStatement = Parser.ConvertToPostFix(inFixInput);
			var separatedTokens = postFixStatement.Split(Constants.Miscellaneous.RpnSeparator);
			var operatorFactory = new OperatorFactory();
		
			if (Logger != null)
				Logger.AddLog(Logging.ConvertedStatement,
					postFixStatement,
					Environment.NewLine);

			foreach(string? token in separatedTokens)
			{
				//Sanity check, probably we can delete it later
				if (token.CompareTo("") == 0)
					continue;

				var operatorToken = operatorFactory.GetOperator(token);
				
				if (operatorToken is IExecutableOperator)
					OperatorHandler(operatorToken as IExecutableOperator);

				else if (operatorToken is IFunction)
					FunctionHandler(operatorToken as IFunction);

				else
					ValueHandler(token);
			}

			var evaluationResult = PopValueConvertToDouble();
			LogFinalization(evaluationResult);

			return evaluationResult;
		}

		private void LogFinalization(double result)
		{
			if (Logger != null)
				Logger.AddLog(Logging.FinalResult,
					Logging.LogSeparator,
					result.ToString(),
					Environment.NewLine,
					Environment.NewLine);
		}

		private void ValueHandler(string token)
		{
			double buffer = 0;
			string tokenLog = token;

			if (Constants.MathSymbol.MathSymbolsDictionary.TryGetValue(token, out buffer))
			{
				token = buffer.ToString();
				tokenLog += Logging.LogSeparator;
				tokenLog += token;
			}

			if (Logger != null)
				Logger.AddLog(Logging.ValueToken,
					Logging.LogSeparator,
					tokenLog,
					Environment.NewLine);
			
			valuesStack.Push(token);

		}

		private void FunctionHandler(IFunction function)
		{
			var operand = PopValueConvertToDouble();

			var result = function
				.EvaluateFunction(operand)
				.ToString();

			valuesStack.Push(result);

			LogComputingSteps(operand.ToString(),
				"",
				function.Symbol,
				result);
		}
		private void OperatorHandler(IExecutableOperator executableOperator)
		{
			var operand2 = PopValueConvertToDouble();
			var operand1 = PopValueConvertToDouble();

			var result = executableOperator.Evaluate(operand1, operand2).ToString();

			valuesStack.Push(result);

			LogComputingSteps(operand1.ToString(), 
				operand2.ToString(), 
				executableOperator.Symbol, 
				result);

		}

		private void LogComputingSteps(string operand1, string operand2, string operationSymbol, string operationResult)
		{
			if (Logger == null)
				return;

			Logger.AddLog(Environment.NewLine,
				Logging.OperatorFound,
				Logging.LogSeparator,
				operationSymbol,
				Environment.NewLine,
				Logging.PerfromingEvaluation,
				Environment.NewLine);

			Logger.AddLog(Logging.PopedFromStack,
				Logging.LogSeparator,
				Environment.NewLine);

			Logger.AddLog(Logging.Operand1,
				Logging.LogSeparator,
				operand1.ToString(),
				Environment.NewLine);

			if(operand2 != null && operand2 !="")
				Logger.AddLog(Logging.Operand2,
					Logging.LogSeparator,
					operand2.ToString(),
					Environment.NewLine);

			Logger.AddLog(Logging.OperationSymbol,
				Logging.LogSeparator,
				operationSymbol,
				Environment.NewLine);

			Logger.AddLog(Logging.PushResultToStack,
				Environment.NewLine,
				Environment.NewLine);
		}

		private double PopValueConvertToDouble()
		{
			double result = 0;
			string operand = "";

			if (valuesStack.TryPop(out operand) == false)
				throw new PopOperandException(Errors.PopOperandFailed);

			if (double.TryParse(operand, out result) == false)
				throw new ParseOperandException(Errors.OperandConversionFailed +
					Environment.NewLine + 
					Errors.OperandConversionFailed_ExtendedText);
			
			return result;
		}
	}
}
