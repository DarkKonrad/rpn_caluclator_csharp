﻿using System;
using System.Runtime.Serialization;

namespace RPN_Calculator.Common
{
	[Serializable()]
	public class PopOperandException : Exception
	{
		public PopOperandException(string message) : base(message) { }

		public PopOperandException(string message, Exception innerException) : base(message, innerException) { }

		protected PopOperandException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}