﻿using RPN_Calculator.Interface.Common;
using System;
namespace RPN_Calculator.Common.Operator
{
	public class ModuloOperator : IExecutableOperator
	{
		public ModuloOperator()
		{
			this.Symbol = Constants.OperatorSymbol.Modulo;
			this.Priority = 3;
		}

		public string Symbol { get; }
		public short Priority { get; }

		public double Evaluate(double leftOperand, double rightOperand)
		{
			if (rightOperand == 0)
				throw new System.DivideByZeroException();

			return leftOperand % rightOperand;
		}
	}
}
