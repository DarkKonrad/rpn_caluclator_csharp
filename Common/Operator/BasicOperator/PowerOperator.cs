﻿using System;
using RPN_Calculator.Interface.Common;

namespace RPN_Calculator.Common.Operator
{
	public class PowerOperator : IExecutableOperator
	{
		public PowerOperator()
		{
			this.Symbol = Constants.OperatorSymbol.Power;
			this.Priority = 4;
		}

		public string Symbol { get; }
		public short Priority { get; }

		public double Evaluate(double leftOperand, double rightOperand)
		{
			return Math.Pow(leftOperand, rightOperand);
		}
	}
}
