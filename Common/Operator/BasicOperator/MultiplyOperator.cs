﻿using RPN_Calculator.Interface.Common;
namespace RPN_Calculator.Common.Operator
{
	public class MultiplyOperator : IExecutableOperator
	{
		
		public MultiplyOperator()
		{
			this.Symbol = Constants.OperatorSymbol.Multiply;
			this.Priority = 3;
		}

		public string Symbol { get; }
		public short Priority { get; }

		public double Evaluate(double leftOperand, double rightOperand)
		{
			return leftOperand * rightOperand;
		}
	}
}
