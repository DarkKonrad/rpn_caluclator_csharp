﻿using RPN_Calculator.Interface.Common;

namespace RPN_Calculator.Common.Operator
{
	public class BraceOpenOperator : IOperator
	{
		public BraceOpenOperator()
		{
			this.Symbol = Constants.OperatorSymbol.BraceOpen;
			this.Priority = 1;
		}
		public short Priority { get; }
		public string Symbol { get; }

		public double Evaluate(double leftOperand, double rightOperand)
		{
			throw new System.NotImplementedException();
		}
	}
}
