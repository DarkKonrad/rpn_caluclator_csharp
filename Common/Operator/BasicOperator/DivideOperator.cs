﻿using RPN_Calculator.Interface.Common;
namespace RPN_Calculator.Common.Operator
{
	public class DivideOperator : IExecutableOperator
	{
		public DivideOperator()
		{
			this.Symbol = Constants.OperatorSymbol.Divide;
			this.Priority = 3;
		}

		public string Symbol { get; }
		public short Priority { get; }
		public double Evaluate(double leftOperand, double rightOperand)
		{
			if (rightOperand == 0)
				throw new System.DivideByZeroException();

			return leftOperand / rightOperand;
		}
	}
}
