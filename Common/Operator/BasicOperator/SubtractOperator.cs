﻿using RPN_Calculator.Interface.Common;

namespace RPN_Calculator.Common.Operator
{
	public class SubtractOperator : IExecutableOperator
	{
		public SubtractOperator()
		{
			this.Symbol = Constants.OperatorSymbol.Subtract;
			this.Priority = 2;
		}

		public string Symbol { get; }
		public short Priority  {get;}

		public double Evaluate(double leftOperand, double rightOperand)
		{
			return leftOperand - rightOperand;
		}
	}
}
