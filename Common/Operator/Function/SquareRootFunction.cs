﻿using RPN_Calculator.Interface.Common;
using System;

namespace RPN_Calculator.Common.Function
{
	class SquareRootFunction : IFunction
	{
		public SquareRootFunction()
		{
			Priority = Constants.Miscellaneous.FunctionPriority;
			Symbol = Constants.FunctionSymbol.SquareRoot;
		}

		public short Priority { get; }

		public string Symbol { get; }

		public double EvaluateFunction(double argument)
		{
			return Math.Sqrt(argument);
		}
	}
}
