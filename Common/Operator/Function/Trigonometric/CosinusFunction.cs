﻿using RPN_Calculator.Interface.Common;
using System;
namespace RPN_Calculator.Common.Function
{
	public class CosinusFunction : IFunction
	{
		public CosinusFunction()
		{
			Priority = Constants.Miscellaneous.FunctionPriority;
			Symbol = Constants.FunctionSymbol.Cosinus;
		}

		public short Priority { get; }

		public string Symbol { get; }

		public double EvaluateFunction(double argument)
		{
			return Math.Cos(argument);
		}
	}
}

