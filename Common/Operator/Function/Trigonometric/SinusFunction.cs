﻿using RPN_Calculator.Interface.Common;
using System;
namespace RPN_Calculator.Common.Function
{
	public class SinusFunction : IFunction
	{
		public SinusFunction()
		{
			Priority = Constants.Miscellaneous.FunctionPriority;
			Symbol = Constants.FunctionSymbol.Sinus;
		}

		public short Priority { get; }

		public string Symbol { get; }

		public double EvaluateFunction(double argument)
		{
			return Math.Sin(argument);
		}
	}
}
