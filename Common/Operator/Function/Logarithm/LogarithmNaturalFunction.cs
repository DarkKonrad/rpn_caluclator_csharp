﻿using RPN_Calculator.Interface.Common;
using System;
namespace RPN_Calculator.Common.Function
{
	public class LogarithmNaturalFunction : IFunction
	{
		public LogarithmNaturalFunction()
		{
			Priority = Constants.Miscellaneous.FunctionPriority;
			Symbol = Constants.FunctionSymbol.LogNatural;
		}

		public short Priority { get; }

		public string Symbol { get; }

		public double EvaluateFunction(double argument)
		{
			return Math.Log(argument);
		}
	}
}
