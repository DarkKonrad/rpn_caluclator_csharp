﻿using RPN_Calculator.Interface.Common;
using System;
namespace RPN_Calculator.Common.Function
{
	public class Logarithm2Function : IFunction
	{
		public Logarithm2Function()
		{
			Priority = Constants.Miscellaneous.FunctionPriority;
			Symbol = Constants.FunctionSymbol.Log2;
		}

		public short Priority { get; }

		public string Symbol { get; }

		public double EvaluateFunction(double argument)
		{
			return Math.Log2(argument);
		}
	}
}
