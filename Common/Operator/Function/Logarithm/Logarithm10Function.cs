﻿using RPN_Calculator.Interface.Common;
using System;
namespace RPN_Calculator.Common.Function
{
	public class Logarithm10Function : IFunction
	{
		public Logarithm10Function()
		{
			Priority = Constants.Miscellaneous.FunctionPriority;
			Symbol = Constants.FunctionSymbol.Log10;
		}

		public short Priority { get; }

		public string Symbol { get; }

		public double EvaluateFunction(double argument)
		{
			return Math.Log10(argument);
		}
	}
}
