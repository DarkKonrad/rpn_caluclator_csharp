﻿using RPN_Calculator.Interface.Common;
using RPN_Calculator.Common.Function;

namespace RPN_Calculator.Common.Operator
{
	public class OperatorFactory : IOperatorFactory
	{
	public IOperator GetOperator(string Symbol)
		{
			switch(Symbol)
			{
				// Basic Operators
				case Constants.OperatorSymbol.Add: 
					return new AddOperator();
				case Constants.OperatorSymbol.BraceOpen: 
					return new BraceOpenOperator();
				case Constants.OperatorSymbol.Divide: 
					return new DivideOperator();
				case Constants.OperatorSymbol.Modulo: 
					return new ModuloOperator();
				case Constants.OperatorSymbol.Multiply: 
					return new MultiplyOperator();
				case Constants.OperatorSymbol.Power: 
					return new PowerOperator();
				case Constants.OperatorSymbol.Subtract: 
					return new SubtractOperator();

				//Trigonometric Functions
				case Constants.FunctionSymbol.Sinus:
					return new SinusFunction();
				case Constants.FunctionSymbol.Cosinus:
					return new CosinusFunction();
				case Constants.FunctionSymbol.Tangent:
					return new TangentFunction();
				case Constants.FunctionSymbol.Cotangent:
					return new CotangentFunction();

				// Logarithms
				case Constants.FunctionSymbol.Log10:
					return new Logarithm10Function();
				case Constants.FunctionSymbol.Log2:
					return new Logarithm2Function();
				case Constants.FunctionSymbol.LogNatural:
					return new LogarithmNaturalFunction();

				// Other
				case Constants.FunctionSymbol.SquareRoot:
					return new SquareRootFunction();
				default:
					return null;
			}
		}
	}
}
