﻿using System;
using System.Runtime.Serialization;

namespace RPN_Calculator.Common.Application
{
	[Serializable()]
	public class UnrecognizedArgumentException : Exception
	{
		public UnrecognizedArgumentException(string message) : base(message) { }

		public UnrecognizedArgumentException(string message, Exception innerException) : base(message, innerException) { }

		protected UnrecognizedArgumentException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
