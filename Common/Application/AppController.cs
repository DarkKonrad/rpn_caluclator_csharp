﻿using System;
using RPN_Calculator.Interface.Evaluator;
using Mono.Options;
using System.Diagnostics;
namespace RPN_Calculator.Common.Application
{
	public class AppController
	{
		private IPostFixEvaluator evaluator;
		private bool showSteps;
		private bool toPostFixOnly;
		private bool showVersion;
		private bool showHelp;
		private bool showAvailableOperations;
	
		public AppController(IPostFixEvaluator evaluator)
		{
			this.evaluator = evaluator;
			showSteps = false;
			toPostFixOnly = false;
			showVersion = false;
			showHelp = false;
			showAvailableOperations = false;
		}

		public void RunApp(string[] args)
		{
			var optionsParser = InitalizeOptionSet();
			var extraArgs = optionsParser.Parse(args);

			if (extraArgs.Count > 1)
			{
				Console.WriteLine(Constants.Messages.Errors.TooManyStatementsToCompute);
				return;
			}

			if (showHelp == true)
			{
				PrintOptions(optionsParser);
				return;
			}
			
			if (showAvailableOperations == true)
			{
				PrintAvailableOperations();
				return;
			}
			
			if(showVersion == true)
			{
				Console.WriteLine(Constants.Miscellaneous.Version);
				return;
			}

			if (extraArgs.Count != 1)
			{
				Console.WriteLine(Constants.Messages.Errors.EmptyOrInvalidInput);
				return;
			}

			if(toPostFixOnly == true)
			{
				try
				{
					Console.WriteLine(evaluator.Parser.ConvertToPostFix(extraArgs[0]));
				}
				catch(Exception e)
				{
					Console.WriteLine(e.Message);

					Debug.WriteLine(e.Message);
					Debug.WriteLine("");
					Debug.WriteLine(e.StackTrace);
				}
				return;
			}

			try
			{
				var result = evaluator.Evaluate(extraArgs[0]);
				Console.WriteLine(result);
				if(showSteps == true)
				{
					var loggedSteps = evaluator.Logger.GetLogs();
					Console.WriteLine();
					Console.WriteLine(loggedSteps);
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);

				Debug.WriteLine(e.Message);
				Debug.WriteLine("");
				Debug.WriteLine(e.StackTrace);
			}
			
		}

		private void PrintAvailableOperations()
		{
			Console.WriteLine(Constants.Messages.HelpMessages.Operations.AvailableOperations);
			Console.WriteLine();

			foreach (string message in Constants.OperatorSymbol.OperatorSymbols)
				Console.WriteLine(Constants.Miscellaneous.Tabulation + message);

			Console.WriteLine();
			Console.WriteLine(Constants.Messages.HelpMessages.Operations.AvailableFunctions);
			Console.WriteLine();

			foreach (string message in Constants.FunctionSymbol.FunctionSymbols)
				Console.WriteLine(Constants.Miscellaneous.Tabulation + message);

			Console.WriteLine();
			Console.WriteLine(Constants.Messages.HelpMessages.Operations.AvailableConstatns);
			Console.WriteLine();

			foreach (string? message in Constants.MathSymbol.MathSymbolsDictionary.Keys)
				if(message != null)
					Console.WriteLine(Constants.Miscellaneous.Tabulation + message);

		}

		private void PrintOptions(OptionSet options)
		{		
			Console.WriteLine(Constants.Messages.HelpMessages.General.Usage);
			Console.WriteLine(Constants.Messages.HelpMessages.General.EnterStatementQuotes);
			Console.WriteLine(Constants.Messages.HelpMessages.General.Example);
			Console.WriteLine(Constants.Messages.HelpMessages.General.TrigonRadianInfo);
			Console.WriteLine();
			Console.WriteLine(Constants.Messages.HelpMessages.General.AvailableOptions);
			options.WriteOptionDescriptions(Console.Out);
			
		}

		private OptionSet InitalizeOptionSet()
		{
			var optionSet = new OptionSet()
			{
				{
					Constants.ConsoleOptions.FormattedOptions.Version,
					Constants.Messages.ExtentedOptionDescriptions.Version,
					o => showVersion = o != null
				},

				{
					Constants.ConsoleOptions.FormattedOptions.ShowSteps,
					Constants.Messages.ExtentedOptionDescriptions.ShowSteps,
					o => showSteps = o !=null
				},

				{
					Constants.ConsoleOptions.FormattedOptions.ConversionOnly,
					Constants.Messages.ExtentedOptionDescriptions.ConversionOnly,
					o => toPostFixOnly = o != null
				},

				{
					Constants.ConsoleOptions.FormattedOptions.Help,
					Constants.Messages.ExtentedOptionDescriptions.Help,
					o => showHelp = o != null
				},

				{
					Constants.ConsoleOptions.FormattedOptions.ShowAvailableOperations,
					Constants.Messages.ExtentedOptionDescriptions.ShowAvailableOperations,
					o => showAvailableOperations = o != null
				}
			};

			return optionSet;
		}
	}
}
