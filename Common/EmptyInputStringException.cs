﻿using System;
using System.Runtime.Serialization;

namespace RPN_Calculator.Common
{
	[Serializable()]
	public class EmptyInputStringException : Exception
	{
		public EmptyInputStringException(string message) : base(message) { }

		public EmptyInputStringException(string message, Exception innerException) : base(message, innerException) { }

		protected EmptyInputStringException(SerializationInfo info, StreamingContext context) : base(info, context) { }
		
	}
}
