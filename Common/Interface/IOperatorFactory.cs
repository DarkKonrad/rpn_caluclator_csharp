﻿namespace RPN_Calculator.Interface.Common
{
	public interface IOperatorFactory 
	{
		IOperator GetOperator(string symbol);
	}
}
