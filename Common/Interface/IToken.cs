﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPN_Calculator.Interface.Common
{
	public interface IToken
	{
		string Symbol { get; }
	}
}
