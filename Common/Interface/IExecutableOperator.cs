﻿
namespace RPN_Calculator.Interface.Common
{
	public interface IExecutableOperator : IOperator
	{
		double Evaluate(double leftOperand, double rightOperand);
	}
}
