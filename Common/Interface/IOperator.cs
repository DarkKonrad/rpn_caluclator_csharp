﻿
namespace RPN_Calculator.Interface.Common
{
	public interface IOperator : IToken
	{
		public short Priority { get; }
	}
}
