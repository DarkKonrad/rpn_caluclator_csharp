﻿
namespace RPN_Calculator.Interface.Common
{
	public interface IFunction : IOperator
	{
		public double EvaluateFunction(double argument);
	}
}
