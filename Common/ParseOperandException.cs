﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace RPN_Calculator.Common
{
	[Serializable()]
	public class ParseOperandException : Exception
	{
		public ParseOperandException(string message) : base(message) { }

		public ParseOperandException(string message, Exception innerException) : base(message, innerException) { }

		protected ParseOperandException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
