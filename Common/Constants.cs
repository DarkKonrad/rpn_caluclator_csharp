﻿using System;
using System.Collections.Generic;

namespace RPN_Calculator.Common
{
	public static class Constants
	{
		public static class OperatorSymbol
		{
			public const string Add = "+";
			public const string Subtract = "-";
			public const string Divide = "/";
			public const string Multiply = "*";
			public const string Power = "^";
			public const string Modulo = "%";
			public const string BraceOpen = "(";
			public const string BraceClose = ")";
			
			public static readonly IList<string> OperatorSymbols = new List<string>() 
			{ Add, Subtract, Divide, Multiply, Power, Modulo, BraceOpen, BraceClose };
		}

		public static class MathSymbol
		{
			public const string Pi = "pi";
			public const string Euler = "e";
			public static Dictionary<string, double> MathSymbolsDictionary = new Dictionary<string, double>()
			{
				{Pi,System.Math.PI }, 
				{Euler,System.Math.E}
			};
		}

		public static class FunctionSymbol
		{
			public const string Sinus = "sin";
			public const string Cosinus = "cos";
			public const string Tangent = "tan";
			public const string Cotangent = "ctg";

			public const string LogNatural = "ln";
			public const string Log2 = "log2";
			public const string Log10 = "log10";

			public const string SquareRoot = "sqrt";

			public static readonly IList<string> FunctionSymbols = new List<string>()
			{ Sinus, Cosinus, Tangent, Cotangent, LogNatural, Log2, Log10, SquareRoot};
		}


		public static class Miscellaneous
		{
			public const string RpnSeparator = " ";
			public const string Version = "1.0";
			public const string Tabulation = "   ";
			public const string Space = " ";
			public const short FunctionPriority = 4;
		}

		public static class Messages
		{
			public static class Errors
			{
				public const string OperatorNotFound = "Desired RPN calculator's operator not found";
				public const string PopOperandFailed = "Failed to execute pop operation for string-operand stack";
				public const string OperandConversionFailed = "Error occured when tried to parse operand to double type";
				public const string OperandConversionFailed_ExtendedText = "It is possible that parser received unsupported symbol ";
				public const string EmptyInputString = "Expected string with statement but received empty string";
				public const string UnrecognizedArgument = "Unrecognized or misspelled argument";
				public const string TooManyStatementsToCompute = "Too many statements to compute or missing quote. Please enter one statement at a time.";
				public const string EmptyOrInvalidInput = "Empty or invalid input string";
			}
	
			public static class Logging
			{
				public const string ConvertedStatement = "Statement converted to PostFix Notation: ";
				public const string LogSeparator = ": ";
				public const string PopedFromStack = "Pop-ed value from stack";
				public const string Operand1 = "Operand_1";
				public const string Operand2 = "Operand_2";
				public const string OperationSymbol = "Operation symbol";
				public const string OperationResult = "Operation result";
				public const string PushResultToStack = "Pushing result to stack";
				public const string ValueToken = "Token";
				public const string OperatorFound = "Founded Operator";
				public const string PerfromingEvaluation = "PerformingEvaluation...";
				public const string FinalResult = "Final result pop-ed from stack";
			}

			public static class ExtentedOptionDescriptions
			{
				public const string Version = "Prints current version of calculator.";
				public const string ConversionOnly = "Returns input statement converted to postfix without evaluating.";
				public const string ShowSteps = "Prints evalutaions steps to output. ";
				public const string Help = "Prints avalible commnads.";
				public const string ShowAvailableOperations = "Print All Available: Operation, Function and Constant symbols, that can be used in program";
			}

			public static class HelpMessages
			{
				public static class General
				{
					public const string Usage = "Usage: pxcalc [OPTIONS]+ statement to compute.";
					public const string EnterStatementQuotes = "Please enter statement with quote.";
					public const string Example = "Example: \"sin(30) + cos(90)\" ";
					public const string TrigonRadianInfo = "Please note that trigonometric function argument are interpreted as radians.";
					public const string AvailableOptions = "Available Options:";
				}
				public static class Operations
				{
					public const string AvailableOperations = "Available Operations: ";
					public const string AvailableFunctions = "Available Functions: ";
					public const string AvailableConstatns = "Available Constatns: ";
				}
			}
		}
		public static class ConsoleOptions
		{
			public const string Help = "help";
			public const string ConversionOnly = "conversion-only";
			public const string Version = "version";
			public const string ShowSteps = "show-steps";
			public const string ShowAvailableOperations = "show-operations";

			public static readonly IList<string> AllCommands = new List<string>()
			{ Help, ConversionOnly, Version, ShowSteps, ShowAvailableOperations };

			public const string OptionSeparator = "|";

			public static class LetterShortcut
			{
				public const string Help = "h";
				public const string ConversionOnly = "c";
				public const string Version = "v";
				public const string ShowSteps = "s";
				public const string ShowAvailableOperations = "o";

				public static readonly IList<string> AllLetterShortcuts = new List<string>()
				{ Help, ConversionOnly, Version, ShowSteps, ShowAvailableOperations };
			}

			public static class FormattedOptions
			{
				public const string Help = ConsoleOptions.Help + OptionSeparator + LetterShortcut.Help;
				public const string ConversionOnly = ConsoleOptions.ConversionOnly + OptionSeparator + LetterShortcut.ConversionOnly;
				public const string Version = ConsoleOptions.Version + OptionSeparator + LetterShortcut.Version;
				public const string ShowSteps = ConsoleOptions.ShowSteps + OptionSeparator + LetterShortcut.ShowSteps;
				public const string ShowAvailableOperations = ConsoleOptions.ShowAvailableOperations + OptionSeparator + LetterShortcut.ShowAvailableOperations;

				public static readonly IList<string> AllFormattedOptions = new List<string>()
				{ Help, ConversionOnly, Version, ShowSteps,ShowAvailableOperations };
			}

		}
	}
}
