﻿using RPN_Calculator.Parser;
using RPN_Calculator.RpnEvaluator;
using RPN_Calculator.Common.Operator;
using RPN_Calculator.Common.Application;

namespace RPN_Calculator
{
	class Program
	{
		static void Main(string[] args)
		{
			var parser = new PostFixParser(new OperatorFactory());

			var evaluator = new Evaluator(parser, 
				new OperatorFactory(), 
				new StepLogger());

			var appController = new AppController(evaluator);

			appController.RunApp(args);
			
		}
	}
}
